/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TextInput,
  TouchableOpacity,
} from 'react-native';
const width = Dimensions.get('screen').width;
const height = Dimensions.get('screen').height;
const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [active, setActive] = useState(false);
  const [numberValue, setNumberValue] = useState('');

  const backgroundStyle = {
    backgroundColor: 'black',
    flex: 1,
  };
  // const backGroundImage = require('./images/bg.jpeg');
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar backgroundColor={'#000000'} barStyle={'light-content'} />
      <ScrollView
        contentContainerStyle={{flexGrow: 1, alignItems: 'center'}}
        keyboardShouldPersistTaps={'handled'}>
        <View style={styles.imageContainer}>
          <View style={{padding: 20}}>
            <Image source={require('./images/Vector.png')} />
          </View>
          <View>
            <Image
              style={styles.tinyLogo}
              source={require('./images/Tsow_Logo.png')}
            />
          </View>
        </View>
        <Text style={styles.textWelcom}>Welcome.</Text>
        <Text style={styles.text}>
          we will Send You An OTP (One Time Password) On Your Mobile Number.
        </Text>
        <TextInput
          selectionColor={'white'}
          placeholder={!active ? ' Enter Your Number' : null}
          placeholderTextColor={'white'}
          style={styles.input}
          onChangeText={value => {
            console.log(value);
            setNumberValue(value);
          }}
          onFocus={() => {
            setActive(true);
          }}
          onBlur={() => setActive(false)}
          blurOnSubmit={false}
          onSubmitEditing={() => {}}
          returnKeyType={'go'}
          value={numberValue}
          keyboardType={'numeric'}
        />
        <Text style={styles.ErrorText}>Enter 10 Digit Number</Text>
        <TouchableOpacity
          onPress={() => {
            console.log('send OTP');
          }}
          style={styles.BtnStyle}>
          <Text style={styles.btnText}>Send OTP</Text>
        </TouchableOpacity>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  image: {
    height: height,
    width: width,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textWelcom: {
    fontSize: 30,
    paddingBottom: 20,
    fontWeight: '900',
    color: '#fff',
  },
  text: {
    fontSize: 20,
    color: 'white',
    paddingBottom: 40,
    fontWeight: '300',
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  input: {
    borderColor: 'skyblue',
    borderWidth: 1,
    width: '90%',
    borderRadius: 10,
    height: 50,
    textAlign: 'center',
    fontSize: 18,
    color: 'white',
  },
  ErrorText: {
    color: 'red',
    paddingTop: 5,
  },
  BtnStyle: {
    backgroundColor: 'skyblue',
    height: 50,
    paddingHorizontal: 50,
    borderRadius: 30,
    justifyContent: 'center',
    marginTop: 40,
  },
  btnText: {
    color: 'black',
    fontSize: 20,
    fontWeight: '500',
    letterSpacing: 1,
  },
  tinyLogo: {
    // height: 200,
    height: 300,
    width: 300,
  },
  imageContainer: {flexDirection: 'row', justifyContent: 'space-between'},
});

export default App;
